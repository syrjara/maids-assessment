import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../core/services/users.service';
import { CommonModule } from '@angular/common';
import { User } from '../../ngxs/models/user.model';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { trigger, transition, style, stagger, animate, AnimationMetadataType } from '@angular/animations';
import { query } from 'express';

@Component({
  selector: 'app-user-details',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './user-details.component.html',
  styleUrl: './user-details.component.scss',
  providers: [UsersService],

})
export class UserDetailsComponent implements OnInit {
  user_details!: User ;
  isLoading: boolean = true;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UsersService
  ) {
    this.route.params.subscribe(params=>{
      const id=params['id']
      this.userService
        .getUserDetails(id)
        .subscribe({
          next: (res: any) => {
            this.user_details = res.data;
            this.isLoading = false;
          },
          error: (err: any) => {
            this.isLoading = false;
          },
        });
    })
  }
  back() {
    this.router.navigate(['/users']);
  }
  ngOnInit(): void {}
}
