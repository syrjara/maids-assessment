import { CommonModule } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild, inject } from '@angular/core';
import { UsersService } from '../core/services/users.service';
import { MatPaginator, MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { Router } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Observable, map, switchMap, tap } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { UsersAction } from '../ngxs/actions/users.action';
import { UsersState } from '../ngxs/states/users.state';
import { RoundedImageDirective } from '../shared/directives/rounded-image.directive';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    RoundedImageDirective,
  ],
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss',
  providers: [UsersService],
})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  isLoading: boolean = true;
  users$!: Observable<any>;
  total$!: Observable<any>;
  page$!: Observable<any>;
  per_page$!: Observable<any>;
  router=inject(Router)
  store=inject(Store)
  ngOnInit(): void {
    this.users$ = this.store.select(UsersState.getUsers);
    this.total$ = this.store.select(UsersState.getTotal);
    //just to see skeleton loader
    setTimeout(() => {
      this.getUsers(1, 5);
    }, 1000);
  }
  pageChange(page: PageEvent) {
    this.isLoading = true;
    this.getUsers(page.pageIndex + 1, page.pageSize);
  }
  getUsers(page: number, per_page: number) {
    this.store.dispatch(new UsersAction(page, per_page)); //dispatch state by call the action
    this.isLoading = false;
  }
  goToDetails(id: number) {
    this.router.navigate(['users', id]); //navigate to specific user details
  }
}
