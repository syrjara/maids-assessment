import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { User } from '../../ngxs/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  base_url: string = 'https://reqres.in/api/';
  constructor(private http: HttpClient) {}
  getUsers(page: number, per_page: number): Observable<any> {
    return this.http.get(`${this.base_url}users?page=${page}&per_page=${per_page}`)
  }
  getUserDetails(id: string): Observable<any> {
    return this.http.get(`${this.base_url}users/${id}`);
  }
}
