import {
  ChangeDetectorRef,
  Component,
  OnInit,
  importProvidersFrom,
  inject,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { LoadingService } from './core/services/loading.service';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    HttpClientModule,
    RouterOutlet,
    MatToolbarModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    SearchComponent,
    MatProgressBarModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  loading: Subject<boolean>=new Subject<boolean>();
  loadingService = inject(LoadingService);
  ngOnInit(): void {
    this.loadingService.loading$.subscribe((load: boolean) => {
      this.loading.next(load) ;
    });
  }
}
