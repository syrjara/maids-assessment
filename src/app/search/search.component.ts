import { CommonModule } from '@angular/common';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { UsersService } from '../core/services/users.service';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [
    CommonModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.scss',
})
export class SearchComponent implements OnInit {
  searchForm!: FormGroup;
  user: any;
  search = new FormControl();
  constructor(private userService: UsersService, private router: Router) {}
  ngOnInit(): void {
    this.search.valueChanges.subscribe((key: any) => {
      if (this.search.value != '') {
        this.userService.getUserDetails(key).subscribe({
          next: (res: any) => {
            this.user = res.data;
          },
          error: (err: any) => {
            // handling error
            this.user = null; // to prevent display user div if not found
          },
        });
      }
    });
  }
  goToDetails(id: string) {
    this.router.navigate(['/users', id]);
    this.search.setValue(''); //reset form
  }
}
