import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import {
  provideRouter,
  withComponentInputBinding,
  withViewTransitions,
} from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { UsersState } from './ngxs/states/users.state';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { loadingInterceptor } from './core/services/loader.interceptor';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withViewTransitions()), //for smooth transitions
    provideClientHydration(),
    provideAnimations(),
    importProvidersFrom(NgxsModule.forRoot([UsersState])), //for ngxs state management
    provideHttpClient(withInterceptors([loadingInterceptor])),
  ],
};
