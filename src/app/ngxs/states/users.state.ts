import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { UsersAction } from '../actions/users.action';
import { UserStateModel } from '../models/user.model';
import { UsersService } from '../../core/services/users.service';
import { tap } from 'rxjs';

@State<UserStateModel>({
  name: 'users',
  defaults: {
    data: [],
    total: 0,
  },
})
@Injectable()
export class UsersState {
  constructor(private userService: UsersService) {}
  @Action(UsersAction)
  getAllUsers(ctx: StateContext<UserStateModel>, action: UsersAction) {    
    const { page, per_page } = action;
    return this.userService.getUsers(page, per_page).pipe(
      tap((res) => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          data: res.data,
          total: res.total,
        });
      })
    );
  }
  @Selector() // selector of data (users)
  static getUsers(state: UserStateModel) {    
    return state.data;
  }

  @Selector() // selector if total count of users
  static getTotal(state: UserStateModel) {
    return state.total;
  }
}
