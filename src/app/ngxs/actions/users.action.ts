export class UsersAction{
    static readonly type='[Users] Get Users' //here we define state action
    constructor(public page: number, public per_page: number){} //here we pass params for request api
}